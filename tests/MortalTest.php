<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Gamecli\Utils\CombatLog;
use Gamecli\Entity\Orderus;
use Gamecli\Entity\WildBeast;

/**
 * Class MortalTest
 */
class MortalTest extends TestCase
{
    public function testDamage(): void
    {
        $orderus = new Orderus(new CombatLog());
        $orderus->strength = 100;

        $beast = new WildBeast(new CombatLog());
        $beast->health = 500;
        $beast->luck = 0;
        $beast->defence = 50;

        $orderus->setTarget($beast);
        $orderus->strike();

        $this->assertEquals(450, $beast->health);
    }

    public function testDying(): void
    {
        $orderus = new Orderus(new CombatLog());
        $orderus->luck = 0;

        $orderus->receiveDamage(666666);

        $this->assertFalse($orderus->isAlive);
    }

    public function testDodge(): void
    {
        $orderus = new Orderus(new CombatLog());
        $orderus->luck = 100;

        $beast = new WildBeast(new CombatLog());
        $beast->strength = 666666666;

        $beast->setTarget($orderus);
        $beast->attack();

        $this->assertTrue($orderus->isAlive);
    }
}
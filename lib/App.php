<?php
declare(strict_types=1);

namespace Gamecli;

use Gamecli\Game\Game;

/**
 * Class App
 * @package Gamecli
 */
class App
{
    /**
     * Entry point
     */
    public function runCommand(): void
    {
        $game = new Game();
        $game->gameStart();
    }
}
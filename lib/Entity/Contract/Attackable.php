<?php
declare(strict_types=1);

namespace Gamecli\Entity\Contract;

/**
 * Interface Attackable
 * @package Gamecli\Entity\Contract
 */
interface Attackable
{
    /**
     * Basic attack
     */
    public function strike(): void;

    /**
     * Sets target for an attack
     * @param Defendable $target
     */
    public function setTarget(Defendable $target): void;

    /**
     * Performs an attack
     */
    public function attack(): void;

}
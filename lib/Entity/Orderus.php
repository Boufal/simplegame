<?php
declare(strict_types=1);

namespace Gamecli\Entity;

use Gamecli\Utils\CombatLog;

/**
 * Class Orderus
 * @package Gamecli\Entity
 */
class Orderus extends Mortal
{
    public function __construct(CombatLog $combatLog)
    {
        parent::__construct($combatLog);
        $this->name = 'Orderus';
        $this->health = rand(70, 100);
        $this->strength = rand(70, 80);
        $this->defence = rand(45, 55);
        $this->speed = rand(40, 50);
        $this->luck = rand(10, 30);
    }

    public function attack(): void
    {
        $this->combatLog->attackMessage($this->name, $this->target->name);

        if (rand(0, 100) <= 10){
            $this->rapidStrike();
        } else {
            $this->strike();
        }
    }

    public function rapidStrike(): void
    {
        $this->combatLog->rapidStrikeMessage($this->name);
        $this->strike();
        $this->strike();
    }

    /**
     * @param float $enemyHitPower
     */
    public function magicShield(float $enemyHitPower): void
    {
        $this->combatLog->magicShieldMessage($this->name);

        $dmg = $this->evaluateDamage($enemyHitPower);
        $this->receiveDamage($dmg / 2);
    }

    /**
     * @param float $enemyHitPower
     */
    public function receiveHit(float $enemyHitPower): void
    {
        if (rand(0, 100) <= 10){
            $this->magicShield($enemyHitPower);
        } else {
            $dmg = $this->evaluateDamage($enemyHitPower);
            $this->receiveDamage($dmg);
        }
    }
}
<?php
declare(strict_types=1);

namespace Gamecli\Utils;

use Gamecli\Entity\Mortal;

/**
 * Class RollHelper
 * @package Gamecli\Utils
 */
class RollHelper
{
    /**
     * @param Mortal[] $registeredCombatants
     * @return array
     */
    public function rollForInitiative(array $registeredCombatants): array
    {
        $combatants = $this->sortBySpeed($registeredCombatants);
        return $this->getOrderIncludingLuck($combatants);
    }

    /**
     * @param mixed $combatants
     * @return array
     */
    public function getOrderIncludingLuck(array $combatants): array
    {
        $order = [];
        /** @var  $combatantsWithSameSpeed */
        foreach ($combatants as $combatantsWithSameSpeed){
            if (count($combatantsWithSameSpeed) > 1){

                $orderBasedOnLuck = [];
                foreach ($combatantsWithSameSpeed as $single){
                    $orderBasedOnLuck[$single->luck] = $single;
                }

                krsort($orderBasedOnLuck);

                foreach ($orderBasedOnLuck as $single){
                    $order []= $single;
                }
            } else {
                $order []= $combatantsWithSameSpeed[0];
            }
        }
        return $order;
    }

    /**
     * @param Mortal[] $registeredCombatants
     * @return array
     */
    public function sortBySpeed(array $registeredCombatants): array
    {
        $combatants = [];
        foreach ($registeredCombatants as $combatant){
            $combatants[$combatant->speed][] = $combatant;
        }

        krsort($combatants);
        return $combatants;
    }
}
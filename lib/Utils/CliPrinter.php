<?php
declare(strict_types=1);

namespace Gamecli\Utils;

/**
 * Class CliPrinter
 * @package Gamecli\Utils
 */
class CliPrinter
{
    /**
     * @param string $message
     * @return void
     */
    public function out(string $message): void
    {
        echo $message;
    }

    /**
     * @param string $message
     * @return void
     */
    public function display(string $message): void
    {
        $this->out($message . "\n");
    }
}
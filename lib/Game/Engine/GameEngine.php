<?php
declare(strict_types=1);

namespace Gamecli\Game\Engine;

use Gamecli\Entity\Mortal;
use Gamecli\Utils\CliPrinter;
use Gamecli\Utils\CombatLog;
use Gamecli\Utils\RollHelper;

/**
 * Class GameEngine
 * @package Gamecli\Game\Engine
 */
class GameEngine
{
    public $registeredCombatants;
    public $combatantsAlive;
    public $rollHelper;
    public $combatLog;
    public $cliPrinter;

    /**
     * Game constructor.
     */
    public function __construct()
    {
        $this->rollHelper = new RollHelper();
        $this->combatLog = new CombatLog();
        $this->cliPrinter = new CliPrinter();
    }

    /**
     * @return bool
     */
    public function areAllDeadExceptOne(): bool
    {
        if (count($this->combatantsAlive) <= 1){
            $lastCombatant = array_values($this->combatantsAlive);
            $this->cliPrinter->display('Game finished, ' . $lastCombatant[0]->name . ' won!');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Mortal $target
     */
    public function didTargetDie(Mortal $target): void
    {
        if (!$target->isAlive){
            $key = array_search($target, $this->combatantsAlive);
            unset($this->combatantsAlive[$key]);
        }
    }

    /**
     * @param Mortal $combatant
     * @return mixed
     */
    public function selectTargetForAttacker(Mortal $combatant): Mortal
    {
        $key = array_search($combatant, $this->combatantsAlive);
        $aliveCombatants = $this->combatantsAlive;
        unset($aliveCombatants[$key]);

        $randomKeyOfTarget = array_rand($aliveCombatants);
        return $aliveCombatants[$randomKeyOfTarget];
    }
}